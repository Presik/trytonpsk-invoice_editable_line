# This file is part of invoice_editable_line module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateView, StateTransition, Button


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._buttons.update({
            'add_product_form': {
                'invisible': Eval('state') != 'draft',
            }})

    @classmethod
    @ModelView.button_action('invoice_editable_line.wizard_add_product')
    def add_product_form(cls, records):
        pass


class AddProductForm(ModelView):
    'Add Product Form'
    __name__ = 'invoice_editable_line.add_product_form'
    invoice = fields.Many2One('account.invoice', 'Invoice')
    lines = fields.One2Many('account.invoice.line', 'invoice', 'Lines',
        context={
            'invoice': Eval('invoice'),
            },
        depends=['invoice'],)


class WizardAddProduct(Wizard):
    'Wizard Add Product'
    __name__ = 'invoice_editable_line.add_product'
    start = StateView('invoice_editable_line.add_product_form',
        'invoice_editable_line.add_product_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add and New', 'add_new_', 'tryton-go-jump', default=True),
            Button('Add', 'add_', 'tryton-ok'),
        ])
    add_new_ = StateTransition()
    add_ = StateTransition()

    def default_start(self, fields):
        return {
            'invoice': Transaction().context.get('active_id'),
        }

    def add_lines(self):
        Invoice = Pool().get('account.invoice')
        invoice = Invoice(self.start.invoice)
        if invoice.state != 'draft':
            return

        for line in self.start.lines:
            line.invoice = Transaction().context.get('active_id', False)
            line.on_change_product()
            line.save()

    def transition_add_new_(self):
        self.add_lines()
        return 'start'

    def transition_add_(self):
        self.add_lines()
        return 'end'
